package basetest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import utilities.CommonUtility;

public class DriverInit {
	
	public Properties pr;
	public static WebDriver driver;
	
	
	
	@BeforeSuite
	public void loadProperties() throws IOException{
		
		FileInputStream fs = new FileInputStream(new File("C:\\mavenautomation\\src\\main\\java\\properties\\config.properties"));
		pr = new Properties();
		pr.load(fs);
	}
	
	@BeforeTest
	public void loadURL(){
		
		if(pr.get("browser").equals("chrome")){
			System.setProperty("webdriver.chrome.driver", "C:\\mavenautomation\\drivers\\chromedriver.exe");
			
			driver = new ChromeDriver();
			
		}
		else if(pr.get("browser").equals("firefox")){
			
			
			driver = new FirefoxDriver();
			
		}
		
		else {
			
			System.out.println("please specify correct brwoser name");
		}
		
		driver.manage().window().maximize();
		driver.get(pr.getProperty("appUrl"));
		
	}
	
	
	@AfterMethod
	public void failureScreenshot(ITestResult it) throws IOException{
		if(ITestResult.FAILURE == it.getStatus()){
			
			CommonUtility.takesScrrenshot(it.getName());
			
		}
		
		
		
	}
	
	@AfterTest
	public void closeBrowser(){
		
		driver.quit();
	}
	
	

}
