package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;






public class ExcelUtils {
	
	
	
	public String path;
	FileInputStream fs;
	XSSFWorkbook wb;
	XSSFSheet sh;
	XSSFRow rw;
	XSSFCell cell;
	
	
	
	public ExcelUtils(String path) throws IOException{
		this.path = path;
		fs = new FileInputStream(path);
		wb =new XSSFWorkbook(fs);
		
		
	}
	
	
	
	public int getRowCount(String sheetName){
		
		sh = wb.getSheet(sheetName);
		
		return sh.getLastRowNum();
		
		
	}
	
	
	
	
	
	public int getColumnCount(String sheetName){
		
		sh = wb.getSheet(sheetName);
		
		rw = sh.getRow(0);
		
		return rw.getLastCellNum();
	}
	
	
	

	public String getCellData(String sheetName, int rowNum, int colNum) throws IOException{
		
		sh = wb.getSheet(sheetName);
		
		rw = sh.getRow(rowNum);
	cell=	rw.getCell(colNum);
	
	return cell.getStringCellValue();
		
		
	}
	
	

}
