package utilities;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.Select;

import basetest.DriverInit;

public class CommonUtility extends DriverInit {
	
	
	
	
	
	// select value from dropdown
	
	public static  void selectValueFromDropdown(WebElement ele, String value){
		
		Select sel = new Select(ele);
		sel.selectByVisibleText(value);
		
	}
	
	
	public static  void takesScrrenshot(String testCaseName) throws IOException{
		
		TakesScreenshot ts = (TakesScreenshot)driver;
		
		
	File src = 	ts.getScreenshotAs(OutputType.FILE);
	
	FileHandler.copy(src, new File("C:\\mavenautomation\\screenshots\\"+testCaseName+".png"));
	
		
		
		
	}
	
	
	

}
