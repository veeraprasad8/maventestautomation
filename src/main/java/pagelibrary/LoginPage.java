package pagelibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {
	
	WebDriver driver;
	public  LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	
	@FindBy(id="txtUsername")
	WebElement userName;
	
	@FindBy(id="txtPassword")
	WebElement password;
	
	
	@FindBy(id="btnLogin")
	WebElement loginButton;
	
	@FindBy(id="divLogo")
	WebElement logo;
	
	@FindBy(xpath = "//img[@alt='OrangeHRM']")
	WebElement HRMImage;
	
	
	public boolean isLogoVerify(){
		
		
		return logo.isDisplayed();
	}
	public boolean isHRMImgVerify(){
		
		
		return HRMImage.isDisplayed();
	}
	
	public void loginToApplication(String username, String pwd){
		userName.sendKeys(username);
		password.sendKeys(pwd);
		loginButton.click();
		
		
	}
	
	

}
