package pagelibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtility;

public class AdminPage {
	WebDriver driver;
	public  AdminPage(WebDriver driver) {
		this.driver = driver;
	}
	
	
	
	@FindBy(xpath = "//b[text()='Admin']")
	WebElement adminEle;
	
	@FindBy(id = "searchSystemUser_userName")
	WebElement userEle;
	
	@FindBy(id = "searchSystemUser_userType")
	WebElement userRole;
	
	@FindBy(id = "searchSystemUser_status")
	WebElement status;

	@FindBy(xpath = "//td[text()='No Records Found']")
	WebElement errorNoRecords;
	
	
	@FindBy(id = "searchBtn")
	WebElement searchBtn;
	
	public void serchUserData(String valueToBeSerach, String userRoleValue, String stausValue){
		adminEle.click();
		userEle.sendKeys(valueToBeSerach);
		CommonUtility.selectValueFromDropdown(userRole, userRoleValue);
		CommonUtility.selectValueFromDropdown(status, stausValue);
		
		searchBtn.click();
		
		
	}
	
	public boolean isErrorMsgDIsplyaedWithNoRecords(){
		
		return errorNoRecords.isDisplayed();
	}
	
	
	
}
