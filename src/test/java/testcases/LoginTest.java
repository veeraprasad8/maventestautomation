package testcases;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import basetest.DriverInit;
import pagelibrary.AdminPage;
import pagelibrary.LoginPage;
import utilities.ExcelUtils;

public class LoginTest extends DriverInit{
	
	LoginPage login;
	AdminPage admin;
	String path = "C:\\mavenautomation\\src\\main\\java\\testdata\\Book1.xlsx";
	
	String sheetName = "Sheet1";
	@Test(priority = 0, dataProvider = "daata")
	public void verifyLogin(String username, String pwd) throws IOException{
		
		
		login = PageFactory.initElements(driver, LoginPage.class);
		login.loginToApplication(username, pwd);
		
	}
	
	
	
	@Test(priority = 1)
	public void verifyAdminPageVerify(){
		
		admin = PageFactory.initElements(driver, AdminPage.class);
		
		admin.serchUserData("abc", "ESS", "Enabled");
		
		Assert.assertTrue(admin.isErrorMsgDIsplyaedWithNoRecords());
		
		
	}
	
	@DataProvider(name = "daata")
	public Object[][] getData() throws IOException{
		
		ExcelUtils ex = new ExcelUtils(path);
		int rowCount = ex.getRowCount(sheetName);
		int colCount = ex.getColumnCount(sheetName);
		
		Object[][] data = new Object [rowCount][colCount];
		
		for(int i =1;i<=rowCount;i++){
			
		for(int j = 0;j<colCount;j++){
			
			data[i-1][j] = ex.getCellData(sheetName, i, j);
		}
		}
		
		
		return data;		
		
		
	}
	


}
