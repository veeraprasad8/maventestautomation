package testcases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utilities.ExcelUtils;

public class TestDataTest {
	String path = "C:\\mavenautomation\\src\\main\\java\\testdata\\Book1.xlsx";
	
	String sheetName = "Sheet1";
	
	@Test(dataProvider = "daata")//test
	public void testDataVerify(String uname, String pwd) throws IOException{
		System.out.println(uname);
		System.out.println(pwd);
		
	}
	

	
	
	@DataProvider(name = "daata")
	public Object[][] getData() throws IOException{
		
		ExcelUtils ex = new ExcelUtils(path);
		int rowCount = ex.getRowCount(sheetName);
		int colCount = ex.getColumnCount(sheetName);
		
		Object[][] data = new Object [rowCount][colCount];
		
		for(int i =1;i<=rowCount;i++){
			
		for(int j = 0;j<colCount;j++){
			
			data[i-1][j] = ex.getCellData(sheetName, i, j);
		}
		}
		
		
		return data;		
		
		
	}
}
